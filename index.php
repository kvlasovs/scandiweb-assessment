<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Product list</title>
  <link rel="stylesheet" href="/css/style.css">
 
</head>

<body class="body">

  <?php
  $hostname = "localhost";
  $dbname = "kvlasovs_scandiweb";
  $username = "kvlasovs_scandiweb";
  $password = "5QCBDx0RT5";

  $conn = mysqli_connect($hostname, $username, $password, $dbname);
  if ($conn->connect_error) {
    echo 'Error number:' . $conn->connect_errno . '<br>';
    echo 'Error: ' . $conn->connect_error;
  }
  ?>

  <?php
  if (isset($_POST['submit'])) {
    if (isset($_POST['id'])) {
      foreach ($_POST['id'] as $id) {
        $query = "DELETE FROM product WHERE id='$id'";
        mysqli_query($conn, $query);
      }
    }
  }

  $sql = "SELECT * FROM product";
  $result = mysqli_query($conn, $sql);
  ?>

<header class="header">
  <div class="container">
    <div class="header__inner">
      <h2 class="header__inner-title">Product List</h2>
      <div class="header__inner-btns">
        <a href="add-product.php">
          <button>ADD</button>
        </a>
        <form class="form" method="post" action="index.php">
        <input id="delete-product-btn" name="submit" type="submit" value="MASS DELETE">
      </div>
    </div>
</header>

<main class="main">
<div class="container">
      <div class="product-items">
        <?php
        while ($row = mysqli_fetch_array($result)) {
        ?>
          <div class="product-item">
            <input class="delete-checkbox" type="checkbox" name="id[]" value="<?= $row['id']; ?>">
            <div class="product-info">

              <p class="sku"><?php echo $row['sku']; ?></p>
              <p class="name"><?php echo $row['name']; ?></p>
              <p class="price"><?php echo $row['price']; ?> $</p>
              <?php if ($row['size']) : ?>
                <p class="size">Size: <?php echo $row['size']; ?> MB</p>
              <?php elseif ($row['weight']) : ?>
                <?php echo '<p class="weight"> Weight: ' . $row['weight'] . 'KG </p>' ?>
              <?php else : ?>
                <?php echo '<p class="dimensions"> Dimensions: ' . $row['height'] . 'x' . $row['width'] . 'x' . $row['length'] . ' </p>' ?>
              <?php endif; ?>


            </div>
          </div>
        <?php
        }
        ?>
    </form>
    <?php
    mysqli_close($conn);
    ?>
  </div>
</div>
</main>

  <footer class="footer">

    <div class="container">
      <p class="footer__inner">Scandiweb Test assigment</p>
    </div>

  </footer>
</body>

</html>