<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Product Add</title>
  <link rel="stylesheet" href="/css/style.css">
</head>

<body class="body">
  <?php
  $mysql = new mysqli("localhost", "kvlasovs_scandiweb", "5QCBDx0RT5", "kvlasovs_scandiweb");
  $mysql->query("SET NAMES 'utf8'");
  
  if ($mysql->connect_error) {
    echo 'Error number:' . $mysql->connect_errno . '<br>';
    echo 'Error: ' . $mysql->connect_error;
  } else {
    $mysql->query("CREATE TABLE `product` (
          id INT(11) NOT NULL AUTO_INCREMENT,
          sku VARCHAR(50) NOT NULL,
          name VARCHAR(50) NOT NULL,
          price VARCHAR(50) NOT NULL,
          size VARCHAR(50) NULL,
          height VARCHAR(50) NULL,
          width VARCHAR(50) NULL,
          length VARCHAR(50) NULL,
          weight VARCHAR(50) NULL,
          PRIMARY KEY(id)
        )");
  }
  ?>

  <header class="header">
    <div class="container">
      <div class="header__inner">
        <h2 class="header__inner-title">Product Add</h2>
        <div class="header__inner-btns">
          <a class="header__inner-cancel" href="http://kvlasovs.com/">
          <button id="delete-product-btn">Cancel</button>
          </a>
          <form id="product_form" class="product__form" action="insert.php" method="post">
          <input id="add-product-btn" type="submit" value="Save"></input>
        </div>
      </div>
    </div>
  </header>

  <main class="main">
    <div class="container">
      
        <div class="product_form-top">
          <label class="product__label">
            SKU
            <input id="sku" class="product__input" type="text" name="sku" required>
          </label>
          <label class="product__label">
            Name
            <input id="name" class="product__input" type="text" name="name" required>
          </label>
          <label class="product__label">
            Price
            <input id="price" class="product__input last" type="text" name="price" required>
          </label>
        </div>
        Type Switcher
        <select id="productType" class="div-toggle" data-target=".add_descr">
          <option value="dvd" data-show=".tab-1">DVD</option>
          <option value="furniture" data-show=".tab-2">Furniture</option>
          <option value="book" data-show=".tab-3">Book</option>
        </select>

        <div class="add_descr">

          <div class="tab-1 hide">
            <label class="product__label">
              Size (MB)
              <input id="size" class="product__input" type="number" name="size">
            </label>
            "Please provide avaible free space on DVD"
          </div>

          <div class="tab-2 hide">
            <label class="product__label">
              Height (CM)
              <input id="height" class="product__input" type="number" name="height">
            </label>
            <label class="product__label">
              Width (CM)
              <input id="width" class="product__input" type="number" name="width">
            </label>
            <label class="product__label">
              Length (CM)
              <input id="length" class="product__input" type="number" name="length">
            </label>
            "Please provide dimensions in HxWxL format"
          </div>

          <div class="tab-3 hide">
            <label class="product__label">
              Weight (KG)
              <input id="weight" class="product__input" type="number" name="weight">
            </label>
            "Please provide weight"
          </div>

        </div>

        
      </form>
    </div>
  </main>

  <footer class="footer">

    <div class="container">
      <p class="footer__inner">Scandiweb Test assigment</p>
    </div>

  </footer>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="/js/main.js"></script>
</body>

</html>